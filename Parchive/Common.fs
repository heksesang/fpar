module Parchive.Common

let memoize fn =
  let cache =
    new System.Collections.Generic.Dictionary<_, _>()

  let add x =
    let v = fn x
    cache.Add(x, v)
    v

  fun x ->
    match cache.TryGetValue x with
    | true, v -> v
    | false, _ -> add x