module Parchive.Math

let inline modulo n m = ((n % m) + m) % m

module Vector =
  let inline dot xs ys =
    List.reduce
    <| (+)
    <| (List.map2 <| (*) <| xs <| ys)

module Matrix =
  let identity n =
    List.init n
    <| fun x -> List.init n <| fun y -> if x = y then 1 else 0

  let rec transpose =
    function
    | (_ :: _) :: _ as m ->
      List.map List.head m
      :: transpose (List.map List.tail m)
    | _ -> []

  let inline multiply xs ys =
    List.map
    <| fun col -> List.map <| (fun row -> Vector.dot row col) <| xs
    <| transpose ys

  let inline inverse m =
    let inner x y m =
      List.removeAt y (List.map <| List.removeAt x <| m)

    let rec minor m =
      match m with
      | m ->
        List.mapi
        <| fun y row ->
              List.init
              <| List.length row
              <| fun x ->
                  match inner x y m with
                  | [ [ b ] ] -> b
                  | m0 ->
                    List.reduce
                    <| (+)
                    <| (List.map2
                        <| (*)
                        <| (List.item 0 <| m0)
                        <| (List.item 0 <| minor m0))
        <| m

    minor m |> transpose

  let print mat =
    let len = List.length mat

    for i, row in (List.indexed mat) do
      let strings =
        List.map <| (fun x -> sprintf "%2O" x) <| row

      let row = String.concat " " strings

      if i = 0 then
        printfn $"[ {row}"
      else if i = len - 1 then
        printfn $"  {row} ]"
        printfn ""
      else
        printfn $"  {row}"