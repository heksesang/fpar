﻿namespace Parchive

module GF =
  open Common
  open Math

  type Table = int * int

  module Table =
    let inline order table = pown <| 2 <| fst table
    let inline limit table = (order table) - 1

  module Log =
    let shift table value =
      let shifted = value <<< 1

      if shifted &&& Table.order table <> 0 then
        shifted ^^^ snd table
      else
        shifted

    let logarithms =
      memoize
      <| fun table ->
           let rec gen table log =
             match log with
             | h :: t when List.length t < Table.limit table - 1 -> gen table <| shift table h :: h :: t
             | t -> t

           let lim = Table.limit table
           let inverseLookup = List.rev <| 0 :: gen table [ 1 ]

           let lookup =
             lim
             :: lim
                :: [ for index in 2 .. lim do
                       yield List.findIndex (fun i -> i = index) inverseLookup ]

           lookup, inverseLookup

  type Symbol =
    { Value: int
      Table: Table }

    override this.ToString() =
      $"{this.Value} in table GF({pown 2 (fst this.Table)}) gen {snd this.Table}"

    static member One table = { Value = 1; Table = table }
    static member Zero table = { Value = 0; Table = table }

    static member From table x =
      { Value = modulo x <| Table.limit table
        Table = table }

    static member Lift table x =
      if table = x.Table then
        x
      else
        { Value = modulo x.Value <| Table.limit table
          Table = table }

    static member (+)({ Value = x; Table = table }, { Value = y }) = x ^^^ y |> Symbol.From table
    static member (-)({ Value = x; Table = table }, { Value = y }) = x ^^^ y |> Symbol.From table

    static member (/)({ Value = x; Table = table }, { Value = y }) =
      if x = 0 || y = 0 then
        Symbol.Zero table
      else
        let lim = Table.limit table
        let log, inverseLog = Log.logarithms table

        modulo <| List.item x log - List.item y log <| lim
        |> fun inv -> List.item inv inverseLog
        |> Symbol.From table

    static member (*)({ Value = x; Table = table }, { Value = y }) =
      if x = 0 || y = 0 then
        Symbol.Zero table
      else
        let lim = Table.limit table
        let log, inverseLog = Log.logarithms table

        modulo <| List.item x log + List.item y log <| lim
        |> fun inv -> List.item inv inverseLog
        |> Symbol.From table

    static member Pow({ Value = x; Table = table }, { Value = n }) =
      if x = 0 then
        Symbol.Zero table
      else if n = 0 then
        Symbol.One table
      else
        let log, inverseLog = Log.logarithms table
        let product = (log.Item x) * n
        let lim = Table.limit table

        let sum =
          (product >>> fst table) + (product &&& lim)

        List.item <| modulo sum lim <| inverseLog
        |> Symbol.From table

module RS =
  open GF
  open Math

  // let table = 16, 0x1100B
  let table = 4, 19

  type RSymbol =
    | RSymbol of Symbol
    override this.ToString() =
      match this with
      | RSymbol (x) -> x.Value.ToString()

    static member Zero = RSymbol { Value = 0; Table = table }

    static member One = RSymbol { Value = 1; Table = table }

    static member (*)(x: RSymbol, y: RSymbol) =
      match x, y with
      | RSymbol (x), RSymbol (y) -> RSymbol(x * y)

    static member (/)(x: RSymbol, y: RSymbol) =
      match x, y with
      | RSymbol (x), RSymbol (y) -> RSymbol(x / y)

    static member (+)(x: RSymbol, y: RSymbol) =
      match x, y with
      | RSymbol (x), RSymbol (y) -> RSymbol(x + y)

    static member (-)(x: RSymbol, y: RSymbol) =
      match x, y with
      | RSymbol (x), RSymbol (y) -> RSymbol(x - y)

    static member Pow(x: RSymbol, n: RSymbol) =
      match x, n with
      | RSymbol (x), RSymbol (n) -> RSymbol(x ** n)

    static member From x = RSymbol { Value = x; Table = table }

    static member FromMatrix =
      List.map <| List.map (fun x -> RSymbol.From(x))

    member this.Value =
      match this with
      | RSymbol (x) -> x.Value

  module NumericLiteralN =
    let FromZero () = RSymbol.From(0)
    let FromOne () = RSymbol.From(1)
    let FromInt32 n = RSymbol.From(n)

  [<AutoOpen>]
  module Ops =
    let (<**>) x n =
      (RSymbol.From x ** RSymbol.From n).Value

    let (<*>) x y = (RSymbol.From x * RSymbol.From y).Value

    let (</>) x y = (RSymbol.From x / RSymbol.From y).Value

    let (<+>) x y = (RSymbol.From x + RSymbol.From y).Value

  [<AutoOpen>]
  module Internal =
    open Ops

    let constantCount = (pown 2 <| fst table) / 2

    let missingExponents es = List.length es < constantCount

    let exponentWithOrder65535 exp =
      exp % 3 <> 0
      && exp % 5 <> 0
      && exp % 17 <> 0
      && exp % 257 <> 0

    let rec thisOrNextExponent exp =
      if exponentWithOrder65535 exp then
        exp
      else
        thisOrNextExponent (exp + 1)

    let exponents =
      let rec run es e =
        let e = thisOrNextExponent e

        match es with
        | es when missingExponents es -> run <| e :: es <| e + 1
        | _ -> es

      run [] 1

    let constants = List.map <| (<**>) 2 <| exponents

    let createChecksumDevice constant exponent data =
      List.map <| (<*>) (constant <**> exponent) <| data

    let vandermondeMatrix nE nC =
      (List.map
       <| fun e -> List.map (fun c -> c <**> e) (List.take nC constants)
       <| [ 0 .. nE - 1 ])

    let combineMatrix x y = x @ y |> RSymbol.FromMatrix

    module List =
      let removeAtMany ns list =
        ns
        |> List.sort
        |> List.mapi (fun i n -> n - i)
        |> List.foldBack List.removeAt
        <| list

  let createRecoveryDevices n data =
    List.map
    <| fun exponent ->
         List.reduce
         <| fun sum checksum -> List.map2 (<+>) sum checksum
         <| (List.map
             <| fun (constant, device) -> createChecksumDevice constant exponent device
             <| List.zip (List.take (List.length data) constants) data)
    <| List.init n id

  let restoreDevices data recovery =
    let missing = List.ofSeq <| Map.keys recovery
    let recovery = List.ofSeq <| Map.values recovery

    let n = List.length data + List.length recovery

    let a =
      combineMatrix
      <| Matrix.identity n
      <| vandermondeMatrix (List.length recovery) n

    let e = combineMatrix data recovery

    let a' =
      List.removeAtMany missing a
      |> List.take n
      |> Matrix.inverse

    let e' = List.take n e

    List.map
    <| List.map (fun (x: RSymbol) -> x.Value)
    <| Matrix.multiply a' e' |> Matrix.transpose
