namespace Parchive.Test

open Parchive.RS
open Expecto
open TickSpec.Functional

module RoundtripSteps =
  let performStep (missing, data, recovery) =
    function
    | Given "you have the data \\[ ((?:\\d+ )+)\\] across (\\d+) devices?" [ IntArray values; Int n ] ->
      List.empty, List.chunkBySize (List.length values / n) values, recovery
    | Given "you create (\\d+) recovery devices?" [ Int n ] -> missing, data, createRecoveryDevices n data
    | Given "you are missing devices \\[ ((?:\\d+ )+)\\]" [ IntArray values ] ->
      let data =
        values
        |> List.sort
        |> List.mapi (fun i n -> n - i)
        |> List.foldBack List.removeAt
        <| data

      values, data, recovery
    | When "you recover with (\\d+) recovery devices?" [ Int n ] ->
      let restored =
        List.length missing |> List.take <| recovery
        |> List.zip missing
        |> Map.ofList
        |> restoreDevices data

      List.empty, restored, recovery
    | Then "you have the data \\[ ((?:\\d+ )+)\\] across (\\d+) devices?" [ IntArray values; Int n ] ->
      Expect.sequenceEqual data (List.chunkBySize (List.length values / n) values)
      <| $"Available data"

      Expect.equal n (List.length data)
      <| $"Number of devices"

      missing, data, recovery
    | _ -> notImplemented ()
