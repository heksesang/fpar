﻿module Parchive.Program

open System.Reflection
open Expecto
open TickSpec
open TickSpec.Functional.ConsoleRunner
open Parchive.Test
open Expecto.Logging

let assembly = Assembly.GetExecutingAssembly()
let stepDefinitions = StepDefinitions(assembly)

let featureFromEmbeddedResource (resourceName: string) : TickSpec.Feature =
  let stream =
    assembly.GetManifestResourceStream(resourceName)

  stepDefinitions.GenerateFeature(resourceName, stream)

let testListFromFeature (feature: TickSpec.Feature) : Expecto.Test =
  feature.Scenarios
  |> Seq.map (fun scenario -> testCase scenario.Name scenario.Action.Invoke)
  |> Seq.toList
  |> testList feature.Name
  |> testSequenced

let featureTest (resourceName: string) =
  (assembly.GetName().Name + "." + resourceName)
  |> featureFromEmbeddedResource
  |> testListFromFeature

[<Tests>]
let roundtripTests =
  run "Roundtrip.feature" RoundtripSteps.performStep (fun () -> List.empty, List.empty, List.empty)

[<EntryPoint>]
let main args =
  runTestsInAssembly { defaultConfig with verbosity = LogLevel.Fatal } args
