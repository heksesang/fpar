Feature: Break data and recover it

Background:
  Given you have the data [ 3 9 1 4 3 0 12 9 8 ] across 3 devices
  Given you create 2 recovery devices

Scenario 1: Successful recovery
  Given you are missing devices [ 1 ]
  When you recover with 1 recovery device
	Then you have the data [ 3 9 1 4 3 0 12 9 8 ] across 3 devices

Scenario 2: Failed recovery
  Given you are missing devices [ 1 2 ]
  When you recover with 1 recovery device
	Then you have the data [ 3 9 1 ] across 1 device