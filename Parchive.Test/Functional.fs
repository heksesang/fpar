namespace TickSpec.Functional

open System.Reflection
open Expecto
open TickSpec

[<AutoOpen>]
module Exceptions =
  exception PendingException of unit
  let pending () = raise <| PendingException()

  let notImplemented () =
    raise <| System.NotImplementedException()

[<AutoOpen>]
module Patterns =
  open System.Text.RegularExpressions

  let private regex input pattern =
    let r = Regex.Match(input, pattern)

    if r.Success then
      Some [ for i = 1 to r.Groups.Count - 1 do
               yield r.Groups.[i].Value ]
    else
      None

  let (|Given|_|) (pattern: string) (step) =
    match step with
    | GivenStep input -> regex input pattern
    | WhenStep _
    | ThenStep _ -> None

  let (|When|_|) (pattern: string) (step) =
    match step with
    | WhenStep input -> regex input pattern
    | GivenStep _
    | ThenStep _ -> None

  let (|Then|_|) (pattern: string) (step) =
    match step with
    | ThenStep input -> regex input pattern
    | GivenStep _
    | WhenStep _ -> None

  let (|Char|) (text: string) = text.[0]
  let (|Int|) s = System.Int32.Parse(s)

  let (|IntArray|) (s: string) =
    s.Split ' '
    |> List.ofArray
    |> List.filter((<>) "")
    |> List.map System.Int32.Parse

[<AutoOpen>]
module ConsoleRunner =
  open System
  open System.IO

  let print color (line: string) =
    let old = Console.ForegroundColor
    Console.ForegroundColor <- color
    printfn "%s" (line.Trim())
    Console.ForegroundColor <- old

  let tryStep performStep state (step, line) =
    let print color = print color line.Text

    try
      let acc = performStep state step
      print ConsoleColor.Green
      acc
    with
    | e ->
      print ConsoleColor.Red
      printfn "Line %d: %A" line.Number e
      reraise ()

  let run feature performStep initState =
    let lines = File.ReadAllLines(feature)
    let feature = FeatureParser.parseFeature lines

    print ConsoleColor.DarkBlue feature.Name

    let tests =
      feature.Scenarios
      |> Seq.filter (fun scenario -> scenario.Tags |> Seq.exists ((=) "ignore") |> not)
      |> Seq.map (fun scenario ->
        test scenario.Name {
          print ConsoleColor.Blue scenario.Name
          scenario.Steps
          |> Array.scan (tryStep performStep) (initState ())
          |> ignore
        }
      )
      |> Seq.toList

    testSequenced <| (testList feature.Name tests)
